Source: python-cpl
Section: python
Priority: optional
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               libcpl-dev,
               python3-all-dev,
               python3-astropy,
               python3-numpy,
               python3-pkg-resources,
               python3-setuptools
Standards-Version: 4.2.1
Homepage: https://pypi.org/project/python-cpl/
Vcs-Browser: https://salsa.debian.org/debian-astro-team/python-cpl
Vcs-Git: https://salsa.debian.org/debian-astro-team/python-cpl.git

Package: python3-cpl
Architecture: any
Depends: python3-astropy,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Suggests: gdb
Description: Control pipeline recipes from the ESO (Python3)
 This module can list, configure and execute CPL-based recipes from Python3.
 The input, calibration and output data can be specified as FITS files
 or as astropy fits objects in memory.
 .
 The Common Pipeline Library (CPL) comprises a set of ISO-C libraries that
 provide a comprehensive, efficient and robust software toolkit. It forms a
 basis for the creation of automated astronomical data-reduction tasks.
 .
 One of the features provided by the CPL is the ability to create
 data-reduction algorithms that run as plugins (dynamic libraries). These are
 called "recipes" and are one of the main aspects of the CPL data-reduction
 development environment.
